<?php
/**
 * @file
 * observation.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function observation_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function observation_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function observation_image_default_styles() {
  $styles = array();

  // Exported image style: observation_thumb.
  $styles['observation_thumb'] = array(
    'label' => 'observation_thumb',
    'effects' => array(
      9 => array(
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 220,
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function observation_node_info() {
  $items = array(
    'observation' => array(
      'name' => t('Observation'),
      'base' => 'node_content',
      'description' => t('Create an <em>observation</em> to log any astronomy you have committed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
