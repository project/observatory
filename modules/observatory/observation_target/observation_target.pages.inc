<?php
/**
 * @file
 * Page callbacks for obsrvation targets.
 */

/**
 * observation_target view callback.
 */
function observation_target_view($observation_target) {
  drupal_set_title(entity_label('observation_target', $observation_target));
  return entity_view('observation_target', array(entity_id('observation_target', $observation_target) => $observation_target), 'full');
}
