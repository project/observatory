<?php

/**
 * Implements hook_boot().
 *
 * Override the Lat/Lon strings to RA/DEC.
 */
function observation_target_boot() {
  global $conf;
  $conf['locale_custom_strings_en']['']['Latitude: !latitude <br/>Longitude: !longitude'] = 'Declination: !latitude <br/>Right Ascension: !longitude';
  $conf['locale_custom_strings_en']['']['Latitude'] = 'DEC';
  $conf['locale_custom_strings_en']['']['Longitude'] = 'RA';
}

/**
 * Implements hook_admin_paths().
 */
function observation_target_admin_paths() {
  if (variable_get('node_admin_theme')) {
    $paths = array(
      'observation_target/*/edit' => TRUE,
      'observation_target/*/delete' => TRUE,
      'observation_target/add' => TRUE,
      'observation_target/add/*' => TRUE,
    );
  return $paths;
}

/**
 * Implements hook_entity_info().
 */
function observation_target_entity_info() {
  $return = array(
    'observation_target' => array(
      'label' => t('Observation Target'),
      'entity class' => 'ObservationTarget',
      'controller class' => 'ObservationTargetController',
      'base table' => 'observation_target',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'otid',
        'bundle' => 'type',
      ),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'bundles' => array(),
      'load hook' => 'observation_target_load',
      'view modes' => array(
        'full' => array(
          'label' => t('Default'),
          'custom settings' => FALSE,
        ),
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'module' => 'observation_target',
      'access callback' => 'observation_target_access',
    ),
  );
  $return['observation_target_type'] = array(
    'label' => t('Observation Target Type'),
    'entity class' => 'ObservationTargetType',
    'controller class' => 'ObservationTargetTypeController',
    'base table' => 'observation_target_type',
    'fieldable' => FALSE,
    'bundle of' => 'observation_target',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'module' => 'observation_target',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/observatory/observation_target-types',
      'file' => 'observation_target.admin.inc',
      'controller class' => 'ObservationTargetTypeUIController',
    ),
    'access callback' => 'observation_target_type_access',
  );

  return $return;
}

/**
 * Implements hook_entity_info_alter().
 */
function observation_target_entity_info_alter(&$entity_info) {
  foreach (observation_target_types() as $type => $info) {
    $entity_info['observation_target']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/observatory/observation_target-types/manage/%observation_target_type',
        'real path' => 'admin/observatory/observation_target-types/manage/' . $type,
        'bundle argument' => 4,
      ),
    );
  }
}

/**
 * Implements hook_menu().
 */
function observation_target_menu() {
  $items = array();

  $items['admin/observatory'] = array(
    'title' => 'Observatory',
    'description' => 'Administer your Drupal observatory.',
    'page callback' => 'observatory_config_page',
    'access arguments' => array('access administration pages'),
    'file' => 'observation_target.admin.inc',
  );

  $items['admin/observatory/observation_target/add'] = array(
    'title' => 'Add Observation Target',
    'page callback' => 'observation_target_admin_add_page',
    'access arguments' => array('administer observation targetss'),
    'file' => 'observation_target.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $observation_target_uri = 'observation_target/%observation_target';
  $observation_target_uri_argument_position = 1;

  $items[$observation_target_uri] = array(
    'title callback' => 'entity_label',
    'title arguments' => array('observation_target', $observation_target_uri_argument_position),
    'page callback' => 'observation_target_view',
    'page arguments' => array($observation_target_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'observation_target', $observation_target_uri_argument_position),
    'file' => 'observation_target.pages.inc',
  );

  $items[$observation_target_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items[$observation_target_uri . '/delete'] = array(
    'title' => 'Delete observation target',
    'title callback' => 'observation_target_label',
    'title arguments' => array($observation_target_uri_argument_position),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('observation_target_delete_form', $observation_target_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'observation_target', $observation_target_uri_argument_position),
    'file' => 'observation_target.admin.inc',
  );

  $items[$observation_target_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('observation_target_form', $observation_target_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'observation_target', $observation_target_uri_argument_position),
    'file' => 'observation_target.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  foreach (observation_target_types() as $type => $info) {
    $items['admin/observatory/observation_target/add/' . $type] = array(
      'title' => check_plain($info->label),
      'page callback' => 'observation_target_add',
      'page arguments' => array(4),
      'access callback' => 'entity_access',
      'access arguments' => array('create', 'observation_target', $type),
      'file' => 'observation_target.admin.inc',
    );
  }

  $items['admin/observatory/observation_target-types/%observation_target_type/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('observation_target_type_form_delete_confirm', 4),
    'access arguments' => array('administer observation_target types'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'observation_target.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function observation_target_permission() {
  $permissions = array(
    'administer observation target types' => array(
      'title' => t('Administer Observation Target types'),
      'description' => t('Allows users to configure observation_target types and their fields.'),
      'restrict access' => TRUE,
    ),
    'create observation targets' => array(
      'title' => t('Create Observation Targets'),
      'description' => t('Allows users to create observation_targets.'),
      'restrict access' => TRUE,
    ),
    'view observation targets' => array(
      'title' => t('View Observation Targets'),
      'description' => t('Allows users to view observation_targets.'),
      'restrict access' => TRUE,
    ),
    'edit any observation targets' => array(
      'title' => t('Edit any Observation Targets'),
      'description' => t('Allows users to edit any observation_targets.'),
      'restrict access' => TRUE,
    ),
    'edit own observation targets' => array(
      'title' => t('Edit own Observation Targets'),
      'description' => t('Allows users to edit own observation_targets.'),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}


/**
 * Implements hook_entity_property_info_alter().
 */
function observation_target_entity_property_info_alter(&$info) {
  $properties = &$info['observation_target_type']['properties'];
  $properties['coordinates'] = array(
    'label' => t("Use coordinates field"),
    'type' => 'integer',
    'description' => t("This observation target type uses celestial coordinates."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer observation target types',
    'schema field' => 'coordinates',
  );
  $properties['catalogues'] = array(
    'label' => t("Use catalogue fields"),
    'type' => 'text',
    'description' => t("This observation target type uses catalogue numbers."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer observation target types',
    'schema field' => 'catalogues',
  );

  $properties = &$info['observation_target']['properties'];
  $properties['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the observation target was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer observation targets',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'description' => t("The date the observation target was most recently updated."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer observation targets',
    'schema field' => 'changed',
  );
  $properties['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the observation target."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer observation targets',
    'required' => TRUE,
    'schema field' => 'uid',
  );
}


/********************************* observation_target API's **********************************/

/**
 * Access callback for observation_target.
 */
function observation_target_access($op, $observation_target, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }

  switch ($op) {
    case 'create':
      return user_access('administer observation targets', $account)
      || user_access('create observation targets', $account);
    case 'view':
      return user_access('administer observation targets', $account)
      || user_access('view observation targets', $account);
    case 'edit':
      return user_access('administer observation targets')
      || user_access('edit any observation targets')
      || (user_access('edit own observation targets') && ($observation_target->uid == $account->uid));
  }
}

/**
 * Load a observation_target.
 */
function observation_target_load($otid, $reset = FALSE) {
  $observation_targets = observation_target_load_multiple(array($otid), array(), $reset);
  return reset($observation_targets);
}

/**
 * Load multiple observation_targets based on certain conditions.
 */
function observation_target_load_multiple($otids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('observation_target', $otids, $conditions, $reset);
}

/**
 * Save observation_target.
 */
function observation_target_save($observation_target) {
  entity_save('observation_target', $observation_target);
}

/**
 * Delete single observation_target.
 */
function observation_target_delete($observation_target) {
  entity_delete('observation_target', entity_id('observation_target', $observation_target));
}

/**
 * Delete multiple observation_targets.
 */
function observation_target_delete_multiple($observation_target_ids) {
 entity_delete_multiple('observation_target', $observation_target_ids);
}


/****************************** observation_target Type API's ********************************/

/**
 * Access callback for observation_target Type.
 */
function observation_target_type_access($op, $entity = NULL) {
  return user_access('administer observation targets');
}

/**
 * Load observation_target Type.
 */
function observation_target_type_load($observation_target_type) {
  return observation_target_types($observation_target_type);
}

/**
 * List of observation_target Types.
 */
function observation_target_types($type_name = NULL) {
  $types = entity_load_multiple_by_name('observation_target_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Save observation_target type entity.
 */
function observation_target_type_save($observation_target_type) {
  entity_save('observation_target_type', $observation_target_type);
}

/**
 * Delete single case type.
 */
function observation_target_type_delete($observation_target_type) {
  entity_delete('observation_target_type', entity_id('observation_target_type', $observation_target_type));
}

/**
 * Delete multiple case types.
 */
function observation_target_type_delete_multiple($observation_target_type_ids) {
  entity_delete_multiple('observation_target_type', $observation_target_type_ids);
}

/**
 * Implements hook_element_info_alter().
 *
 * Inserts our RA/DEC converter before the geofield LAT/LON element validator.
 */
function observation_target_element_info_alter(&$type) {
  $type['geofield_latlon']['#element_validate'] = array('observation_target_radec_validate');
  $type['geofield_latlon']['#process'][] = 'observation_target_radec_process';
}

/**
 * Process helper for a geofield_latlon field with ccs setting.
 */
function observation_target_radec_process($element, &$form_state) {
  $components = array(
    'lat' => array(
      'title' => t('Declination'),
      'description'=> t('Enter as decimal coordinates or in <em>60° 23\' 45.6" SOUTH</em> format.'),
    ),
    'lon' => array(
      'title' => t('Right Ascension'),
      'description'=> t('Enter as decimal coordinates or in <em>11h 23m 45.6s</em> format.'),
    ),
  );

  foreach ($components as $key => $component) {
    $element[$key]['#title'] = $component['title'];
    $element[$key]['#description'] = $component['description'];
    if (!empty($element[$key]['#default_value'])) {
      $where = '';
      if ($key == 'lat') {
        if ($element[$key]['#default_value'] < 0) {
          $where = ' SOUTH';
        }
      }

      $value = geofield_latlon_DECtoCCS(abs($element[$key]['#default_value']), $key);
      $value = strtr($value, array('&deg;' => '°'));

      $element[$key]['#default_value'] = $value . $where;
    }
  }

  return $element;
}

/**
 * Validator for the Messier form element.
 */
function observation_target_messier_validate($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value <= 0 || $value > 110)) {
    form_error($element, t('%name must range from 1 through 110.', array('%name' => $element['#title'])));
  }
}

/**
 * Validator for the Caldwell form element.
 */
function observation_target_caldwell_validate($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value <= 0 || $value > 110)) {
    form_error($element, t('%name must range from 1 through 109.', array('%name' => $element['#title'])));
  }
}

/**
 * Validator for the catalog form element.
 */
function observation_target_catalog_validate($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value <= 0 || $value > 1000000)) {
    form_error($element, t('%name must be a positive integer under 1,000,000.', array('%name' => $element['#title'])));
  }
}

/**
 * Validator for the celestial coordinates.
 */
function observation_target_radec_validate($element, &$form_state) {

  // Rework the RA/DEC to numeric values via helpers from the geofield module.
  $components = array(
    'lat' => array(
      'title' => 'RA',
    ),
    'lon' => array(
      'title' => 'DEC',
    ),
  );

  $values = array();
  foreach ($components as $key => $component) {
    if (!empty($element[$key]['#value'])) {
      $values[$key] = geofield_latlon_DMStoDEC($element[$key]['#value']);
      $temp = geofield_latlon_DECtoCCS($values[$key], $key);
      $element[$key]['#value'] = $values[$key];
    }
  }
  form_set_value($element, $values, $form_state);

  // Fall-through to the original element validator.
  geofield_latlon_element_validate($element, $form_state);
}

/**
 * Implements hook_views_api().
 */
function observation_target_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'observation_target'),
  );
}

/**
 * Implements hook_field_attach_create_bundle().
 *
 * Attaches the observation_target_location field to the bundle.
 */
function observation_target_field_attach_create_bundle($entity_type, $bundle) {
  if ($entity_type == 'observation_target') {

    // Check if we need to add any fields to the bundle. There seems to not
    // be a sensible way of getting these properties via an API call.
    $bundle_info = db_select('observation_target_type', 't')->fields('t')->condition('type', $bundle)->execute()->fetchAssoc();

    // Create the coordinates field instance on the bundle.
    if (!empty($bundle_info['coordinates'])) {
      $instance = array(
        'bundle' => $bundle,
        'default_value' => NULL,
        'deleted' => FALSE,
        'description' => 'The coordinates for the observation target, if applicable.',
        'display' => array(
          'default' => array(
            'label' => 'inline',
            'module' => 'geofield',
            'settings' => array(
              'data' => 'full',
              'field_multiple_limit' => -1,
              'field_multiple_limit_offset' => 0,
              'format' => 'ccs',
              'labels' => 1,
            ),
            'type' => 'geofield_latlon',
            'weight' => 0,
          ),
        ),
        'entity_type' => 'observation_target',
        'field_name' => 'observation_target_ccs',
        'label' => 'Celestial Coordinates',
        'required' => FALSE,
        'settings' => array(),
        'widget' => array(
          'active' => 1,
          'module' => 'geofield',
          'settings' => array(
            'html5_geolocation' => 0,
          ),
          'type' => 'geofield_latlon',
          'weight' => 1,
        ),
      );
      field_create_instance($instance);
    }

    // For each of the catalogue fields, check if the bundle needs one.
    if (!empty($bundle_info['catalogues'])) {
      $catalogues = array_filter(unserialize($bundle_info['catalogues']));

      foreach ($catalogues as $name) {
        $instance = _observation_target_catalogue_fields($name);
        $instance['bundle'] = $bundle;
        field_create_instance($instance);
      }
    }
  }
}

/**
 * Implements hook_field_attach_delete_bundle().
 */
function observation_target_field_attach_delete_bundle($entity_type, $bundle, $instances) {
  if ($entity_type == 'observation_target') {
    // Remove the coordinates field instance if present.
    if ($instance = field_info_instance($entity_type, 'observation_target_ccs', $bundle)) {
      field_delete_instance($instance);
    }

    // Remove any catalogue field instances that were present on the bundle.
    $catalogues = _observation_target_catalogues();
    foreach ($catalogues as $name => $info) {
      if ($instance = field_info_instance($entity_type, 'observation_target_' . $name, $bundle)) {
        field_delete_instance($instance);
      }
    }
  }
}

/**
 * Helper to fetch a list of catalogue names.
 */
function _observation_target_catalogues() {
  return array(
    'messier' => array(
      'type' => 'number',
      'title' => t('Messier Catalogue'),
      'description' => t('The Messier catalogue number for this observation target.'),
      'size' => 3,
      'max' => 110,
      'prefix' => 'M',
    ),
    'caldwell' => array(
      'type' => 'number',
      'title' => t('Caldwell Catalogue'),
      'description' => t('The Caldwell catalogue number for this observation target.'),
      'size' => 3,
      'max' => 109,
      'prefix' => 'C',
    ),
    'ngc' => array(
      'type' => 'number',
      'title' => t('NGC Catalogue'),
      'description' => t('The New Galactic Catalogue number for this observation target.'),
      'size' => 5,
      'prefix' => 'NGC',
    ),
    'ic' => array(
      'type' => 'number',
      'title' => t('IC Catalogue'),
      'description' => t('The Index Catalogue number for this observation target.'),
      'size' => 5,
      'prefix' => 'IC',
    ),
    'hr' => array(
      'type' => 'number',
      'title' => t('HR Catalogue'),
      'description' => t('The Bright Star (Yale) catalogue number for this observation target.'),
      'size' => 6,
      'prefix' => 'HR',
    ),
    'hd' => array(
      'type' => 'number',
      'title' => t('HD Catalogue'),
      'description' => t('The Henry Draper catalogue number for this observation target.'),
      'size' => 6,
      'prefix' => 'HD',
    ),
    'hip' => array(
      'type' => 'number',
      'title' => t('HIPPARCOS Catalogue'),
      'description' => t('The HIPPARCOS catalogue number for this observation target.'),
      'size' => 6,
      'prefix' => 'HIP',
    ),
    'eso' => array(
      'type' => 'text',
      'title' => t('ESO Catalogue'),
      'description' => t('The ESO catalogue number for this observation target.'),
      'size' => 16,
      'prefix' => 'ESO',
    ),
  );
}

function _observation_target_catalogue_fields($label = '') {
  $catalogue_field_instances = &drupal_static(__FUNCTION__);

  // Assemble a list of catalogue field instances.
  if (!isset($catalogue_field_instances)) {
    $catalogues = _observation_target_catalogues();

    $weight = 1;
    foreach ($catalogues as $name => $info) {
      $function = '_observation_target_catalogue_' . $info['type'];
      $catalogue_field_instances[$name] = $function($name, $info);
      $catalogue_field_instances[$name]['widget']['weight'] = ++$weight;
    }
  }

  if (!empty($label)) {
    if (!empty($catalogue_field_instances[$label])) {
      return $catalogue_field_instances[$label];
    }
    return NULL;
  }
  return $catalogue_field_instances;
}

/**
 * Helper to return a number field instance.
 */
function _observation_target_catalogue_number($name, $info) {

  // Maximum value is either passed directory or based on field size.
  if (!empty($info['max'])) {
    $max = $info['max'];
  }
  else {
    if (!empty($info['size'])) {
      $max = pow(10, ($info['size'] + 1));
    }
    else {
      $max = '';
    }
  }

  return array(
    'bundle' => '',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => $info['description'],
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '',
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'observation_target',
    'field_name' => 'observation_target_' . $name,
    'label' => $info['title'],
    'required' => 0,
    'settings' => array(
      'max' => $max,
      'min' => 1,
      'prefix' => !empty($info['prefix']) ? $info['prefix'] : '',
      'suffix' => !empty($info['suffix']) ? $info['suffix'] : '',
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 0,
    ),
  );
}

/**
 * Helper to return a text field instance.
 */
function _observation_target_catalogue_text($name, $info) {
  return array(
    'bundle' => '',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => $info['description'],
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'observation_target',
    'field_name' => 'observation_target_' . $name,
    'label' => $info['title'],
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => $info['size'],
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );
}
