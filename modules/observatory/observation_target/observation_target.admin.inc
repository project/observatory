<?php
/**
 * @file
 * Admin page callbacks for observatory targets.
 */

/**
 * Observatory config landing page.
 */
function observatory_config_page() {
  $item = menu_get_item();

  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('You do not have any observatory items.');
  }
  return $output;
}

/**
 * Generates the observation_target type editing form.
 */
function observation_target_type_form($form, &$form_state, $observation_target_type, $op = 'edit') {

  if ($op == 'clone') {
    $observation_target_type->label .= ' (cloned)';
    $observation_target_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $observation_target_type->label,
    '#description' => t('The human-readable name of this observation target type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($observation_target_type->type) ? $observation_target_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $observation_target_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'observation_target_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this observation target type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($observation_target_type->description) ? $observation_target_type->description : '',
    '#description' => t('Description about the observation target type.'),
  );

  // Only show these options when creating the observation target type. There is no hook to catch editing.
  if ($op == 'add') {
    $form['coordinates'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add coordinates field'),
      '#default_value' => isset($observation_target_type->checkbox) ? $observation_target_type->checkbox : 0,
      '#description' => t('Add a RADEC coordinates field to this observation target type. Useful for deep sky objects, but not for planets or constellations.'),
    );

    foreach (_observation_target_catalogues() as $name => $info) {
      $catalogues[$name] = check_plain($info['title']);
    }
    $form['catalogues'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Add catalogue fields'),
      '#options' => $catalogues,
      '#default_value' => isset($observation_target_type->catalogues) ? unserialize($observation_target_type->catalogues) : array(),
      '#description' => t('Add catalogue fields to this observation target type. Useful for deep sky objects, comets and stars but not for planets or constellations.'),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save observation target type'),
    '#weight' => 40,
  );

  if (!$observation_target_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete observation target type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('observation_target_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Validate handler for creating/editing observation_target_type.
 */
function observation_target_type_form_validate(&$form, &$form_state) {
  // Serialize the catalogue info before saving.
  if (isset($form_state['values']['catalogues'])) {
    $form_state['values']['catalogues'] = serialize(array_filter($form_state['values']['catalogues']));
  }
}

/**
 * Submit handler for creating/editing observation_target_type.
 */
function observation_target_type_form_submit(&$form, &$form_state) {

  $observation_target_type = entity_ui_form_submit_build_entity($form, $form_state);
  observation_target_type_save($observation_target_type);

  // Redirect user back to list of observation_target types.
  $form_state['redirect'] = 'admin/observatory/observation_target-types';
}

/**
 * Submit handler for deleting observation_target_type.
 */
function observation_target_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/observatory/observation_target-types/' . $form_state['observation_target_type']->type . '/delete';
}

/**
 * Observation_target type delete confirm form.
 */
function observation_target_type_form_delete_confirm($form, &$form_state, $observation_target_type) {
  $form_state['observation_target_type'] = $observation_target_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['observation_target_type_id'] = array('#type' => 'value', '#value' => entity_id('observation_target_type', $observation_target_type));
  return confirm_form($form,
    t('Are you sure you want to delete observation target type %title?', array('%title' => entity_label('observation_target_type', $observation_target_type))),
    'observation_target/' . entity_id('observation_target_type', $observation_target_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Observation_target type delete confirm form submit handler.
 */
function observation_target_type_form_delete_confirm_submit($form, &$form_state) {
  $observation_target_type = $form_state['observation_target_type'];
  observation_target_type_delete($observation_target_type);

  watchdog('observation_target_type', '@type: deleted %title.', array('@type' => $observation_target_type->type, '%title' => $observation_target_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $observation_target_type->type, '%title' => $observation_target_type->label)));

  $form_state['redirect'] = 'admin/observatory/observation_target-types';
}

/**
 * Page to select observation_target Type to add new observation_target.
 */
function observation_target_admin_add_page() {
  $content = array();
  foreach (observation_target_types() as $observation_target_type_key => $observation_target_type) {
    $item = array();
    $item['title'] = entity_label('observation_target_type', $observation_target_type);
    $item['href'] = 'admin/observatory/observation_target/add/' . $observation_target_type_key;
    $item['description'] = $observation_target_type->description;
    $content[] = $item;
  }
  if (empty($content)) {
    return t('There are no observation target types, you should <a href="!link">add some</a>.',
      array('!link' => url('admin/observatory/observation_target-types')));
  }
  return theme('node_add_list', array('content' => $content));
}

/**
 * Add new observation_target page callback.
 */
function observation_target_add($type) {
  $observation_target_type = observation_target_types($type);

  $observation_target = entity_create('observation_target', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('observation_target_type', $observation_target_type))));

  $output = drupal_get_form('observation_target_form', $observation_target);

  return $output;
}

/**
 * observation_target Form.
 */
function observation_target_form($form, &$form_state, $observation_target) {
  $form_state['observation_target'] = $observation_target;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $observation_target->title,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $observation_target->description,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $observation_target->uid,
  );

  field_attach_form('observation_target', $observation_target, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save observation target'),
    '#submit' => $submit + array('observation_target_form_submit'),
  );

  // Show Delete button if we edit observation_target.
  $observation_target_id = entity_id('observation_target', $observation_target);
  if (!empty($observation_target_id) && observation_target_access('edit', $observation_target)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('observation_target_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'observation_target_form_validate';

  return $form;
}

function observation_target_form_validate($form, &$form_state) {
  // Nope.
}

/**
 * observation_target submit handler.
 */
function observation_target_form_submit($form, &$form_state) {

  $observation_target = $form_state['observation_target'];
  entity_form_submit_build_entity('observation_target', $observation_target, $form, $form_state);
  observation_target_save($observation_target);

  $observation_target_uri = entity_uri('observation_target', $observation_target);
  $form_state['redirect'] = $observation_target_uri['path'];

  drupal_set_message(t('Observation target %title saved.', array('%title' => entity_label('observation_target', $observation_target))));
}

function observation_target_form_submit_delete($form, &$form_state) {
  $observation_target = $form_state['observation_target'];
  $observation_target_uri = entity_uri('observation_target', $observation_target);
  $form_state['redirect'] = $observation_target_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function observation_target_delete_form($form, &$form_state, $observation_target) {
  $form_state['observation_target'] = $observation_target;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['observation_target_type_id'] = array('#type' => 'value', '#value' => entity_id('observation_target', $observation_target));
  $observation_target_uri = entity_uri('observation_target', $observation_target);
  return confirm_form($form,
    t('Are you sure you want to delete observation target %title?', array('%title' => entity_label('observation_target', $observation_target))),
    $observation_target_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function observation_target_delete_form_submit($form, &$form_state) {
  $observation_target = $form_state['observation_target'];
  observation_target_delete($observation_target);

  drupal_set_message(t('Observation target %title deleted.', array('%title' => entity_label('observation_target', $observation_target))));

  $form_state['redirect'] = '<front>';
}
