api = 2
core = 7.x

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "drupal-org-core.make"

; Download the Observatory install profile and recursively build all its dependencies.
projects[observatory][version] = 1.x-dev
