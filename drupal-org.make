api = 2
core = 7.x

; Contributed modules.

projects[auto_entitylabel][type] = "module"
projects[auto_entitylabel][subdir] = "contrib"
projects[auto_entitylabel][version] = "1.4"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.15"

projects[date][type] = "module"
projects[date][subdir] = "contrib"
projects[date][version] = "2.10"

projects[entity][type] = "module"
projects[entity][subdir] = "contrib"
projects[entity][version] = "1.9"

projects[entitycache][type] = "module"
projects[entitycache][subdir] = "contrib"
projects[entitycache][version] = "1.5"

projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.5"

projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[features][version] = "2.11"

projects[geofield][type] = "module"
projects[geofield][subdir] = "contrib"
projects[geofield][version] = "2.4"

projects[geophp][type] = "module"
projects[geophp][subdir] = "contrib"
projects[geophp][version] = "1.7"

projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.7"

projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.5"

projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[token][version] = "1.7"

projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views][version] = "3.20"
